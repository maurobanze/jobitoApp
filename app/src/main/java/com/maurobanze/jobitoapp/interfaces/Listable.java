package com.maurobanze.jobitoapp.interfaces;

/**
 * Interface for entities that can be listed. It allows common Fragment code to be written
 * once, and adapted if necessary using instanceof. It is implemented by Category and SkillCategory
 */
public interface Listable {

    String getItemTitle();
    String getItemIconPath();
    String getSecondaryText();


}
