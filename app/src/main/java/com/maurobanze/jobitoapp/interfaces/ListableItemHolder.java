package com.maurobanze.jobitoapp.interfaces;

import android.view.View;

/**
 * Interface that must be implemented by holders of a Listable list items.
 * It is used to pass through click events.
 */
public interface ListableItemHolder {

   void onItemClick (Listable item, View itemView, int itemPosition);

}
