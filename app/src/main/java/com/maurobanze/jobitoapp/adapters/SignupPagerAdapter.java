package com.maurobanze.jobitoapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.maurobanze.jobitoapp.SignupFlowActivity;

/**
 * Created by MauroBanze on 12/25/15.
 */
public class SignupPagerAdapter extends FragmentPagerAdapter{

    private final Context context;

    public SignupPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0){

            return new SignupFlowActivity.ScreenOne();
        }else if (position == 1){

            return new SignupFlowActivity.ScreenTwo();
        }

        return new SignupFlowActivity.ScreenOne();

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0: return "Step 1";
            case 1: return "Step 2";
            case 2: return "Step 3";
            default: return "";
        }
    }

    private static final int SECTION_ALL_JOBITOS = 0;
    private static final int SECTION_MY_JOBITOS = 1;
    private static final int SECTION_CREATED_JOBITOS = 2;
    private static final int SECTION_APPLIED_JOBITOS = 3;
}
