package com.maurobanze.jobitoapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maurobanze.jobitoapp.JobitoApp;
import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.interfaces.Listable;
import com.maurobanze.jobitoapp.interfaces.ListableItemHolder;

import java.util.ArrayList;

/**
 * Adapter that defines how a Listable item is transformed into a RecyclerView item to be displayed.
 * It uses a generic item layout file containing a title, subtitle, and a avatar. Furthermore, it
 * expects to receive a ListableItemHolder to which it can notify when events such as clicks occur.
 */
public class ListableListAdapter extends RecyclerView.Adapter<ListableListAdapter.ViewHolder> {

    private ArrayList<? extends Listable> items;
    private Context context;
    private ListableItemHolder myItemHolder;

    public ListableListAdapter(ArrayList<? extends Listable> items, Context context, ListableItemHolder myItemHolder) {


        this.items = items;
        this.context = context;
        this.myItemHolder = myItemHolder;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.skill_category_row, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Listable item = items.get(position);

        holder.textViewItemTitle.setText(item.getItemTitle());

        int pictureRes = context.getResources().
                getIdentifier(item.getItemIconPath(), "drawable", JobitoApp.PACKAGE_NAME);

        Glide.with(context).load(pictureRes).crossFade().centerCrop().into(holder.imageViewItemAvatar);

        holder.itemPosition = position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isShowingSkillCategory() {

        return items.get(0) instanceof SkillCategory;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public int itemPosition;

        public TextView textViewItemTitle;
        public TextView textViewItemSecondaryText;
        public ImageView imageViewItemAvatar;

        public ViewHolder(final View itemView) {
            super(itemView);

            textViewItemTitle = (TextView) itemView.findViewById(R.id.textViewItemTitle);
            textViewItemSecondaryText = (TextView) itemView.findViewById(R.id.textViewItemSecondaryText);
            imageViewItemAvatar = (ImageView) itemView.findViewById(R.id.imageViewCreatorAvatar);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            dispatchItemClick(v);
        }

        private void dispatchItemClick(View view) {

            if (myItemHolder != null) {
                myItemHolder.onItemClick(items.get(itemPosition), view, itemPosition);
            }
        }
    }
}
