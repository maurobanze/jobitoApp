package com.maurobanze.jobitoapp.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maurobanze.jobitoapp.JobitoDetailsActivity;
import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by MauroBanze on 12/29/15.
 */
public class RecommendedJobitoListAdapter extends RecyclerView.Adapter<RecommendedJobitoListAdapter.ViewHolder> {

    private Fragment fragment;
    private ArrayList<Job> jobs;

    public RecommendedJobitoListAdapter(ArrayList<Job> jobs, Fragment fragment) {

        this.jobs = jobs;
        this.fragment = fragment;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.jobito_list_row, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Job job = jobs.get(position);

        holder.textViewTitle.setText(job.getTitle());

        Skill skillNeeded = job.getSkillNeeded();
        SkillCategory skillCategory = skillNeeded.getSkillCategory();
        holder.textViewSkill.setText(skillCategory.getName() + " > " + skillNeeded.getName());

        holder.textViewDateCreated.setText(LocalDB.generateFakeDateRecommmended(position));
        populateStatus(job.getStatus(), holder.textViewStatus);

        Random random = new Random();
        int x = random.nextInt(15);


        if ((x < 3)) {

            Glide.with(fragment).load(R.drawable.avatar_girl).crossFade().centerCrop().into(holder.imageViewCreatorAvatar);

        } else if ((x >= 3) && (x < 6)) {

            Glide.with(fragment).load(R.drawable.avatar_guy_1).crossFade().centerCrop().into(holder.imageViewCreatorAvatar);

        } else if ((x >= 6) && (x < 9)) {

            Glide.with(fragment).load(R.drawable.avatar_girl_2).crossFade().centerCrop().into(holder.imageViewCreatorAvatar);

        } else if ((x >= 9) && (x < 12)) {

            Glide.with(fragment).load(R.drawable.avatar_1).crossFade().centerCrop().into(holder.imageViewCreatorAvatar);

        } else {

            Glide.with(fragment).load(R.drawable.avatar_2).crossFade().centerCrop().into(holder.imageViewCreatorAvatar);
        }


        holder.jobIndex = position;
    }

    private void populateStatus(int status, TextView textViewStatus) {

        textViewStatus.setText(Job.getStatus(status));
        textViewStatus.setBackgroundColor(Job.getColorResource(fragment.getContext(), status));

    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public int jobIndex;

        public TextView textViewSkill;
        public TextView textViewTitle;
        public ImageView imageViewCreatorAvatar;
        public TextView textViewDateCreated;
        public TextView textViewStatus;


        public ViewHolder(final View itemView) {
            super(itemView);

            textViewTitle = (TextView) itemView.findViewById(R.id.textViewJobitoTitle);
            textViewSkill = (TextView) itemView.findViewById(R.id.textViewSkill);
            imageViewCreatorAvatar = (ImageView) itemView.findViewById(R.id.imageViewCreatorAvatar);
            textViewDateCreated = (TextView) itemView.findViewById(R.id.textViewDateCreated);
            textViewStatus = (TextView) itemView.findViewById(R.id.textViewStatus);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            openJobitoDetailsActivity();
        }

        private void openJobitoDetailsActivity() {

            Intent intent = new Intent(fragment.getActivity(), JobitoDetailsActivity.class);
            //intent.putExtra(AgendaFragment.EXTRA_PLAY_INDEX, playIndex);

            fragment.getActivity().startActivity(intent);

        }
    }
}
