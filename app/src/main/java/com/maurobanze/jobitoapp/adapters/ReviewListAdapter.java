package com.maurobanze.jobitoapp.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maurobanze.jobitoapp.JobitoDetailsActivity;
import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.entities.Hire;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 */
public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ViewHolder> {

    private Fragment fragment;
    private ArrayList<Hire> hires;

    public ReviewListAdapter(ArrayList<Hire> hires, Fragment fragment) {

        this.hires = hires;
        this.fragment = fragment;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.worker_review_row, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Hire hire = hires.get(position);

        holder.textViewJobitoTitle.setText(hire.getJob().getTitle());
        holder.textViewDescription.setText(hire.getRatingComment());
        //holder.textViewReviewerAndDate.setText(hire.getJob().getCreator());
        holder.ratingBar.setRating(hire.getRating());


        Random random = new Random();
        int x = random.nextInt(10);


        if ((x < 3)) {

            Glide.with(fragment).load(R.drawable.avatar_girl).crossFade().centerCrop().into(holder.imageViewReviewerAvatar);

        } else if ((x >= 3) && (x < 6)) {

            Glide.with(fragment).load(R.drawable.avatar_girl_2).crossFade().centerCrop().into(holder.imageViewReviewerAvatar);

        } else if (x >= 6) {
            Glide.with(fragment).load(R.drawable.avatar_guy_1).crossFade().centerCrop().into(holder.imageViewReviewerAvatar);
        }


        holder.hireIndex = position;
    }

    @Override
    public int getItemCount() {
        return hires.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public int hireIndex;

        public TextView textViewJobitoTitle;
        public RatingBar ratingBar;
        public TextView textViewDescription;
        public ImageView imageViewReviewerAvatar;
        public TextView textViewReviewerAndDate;


        public ViewHolder(final View itemView) {
            super(itemView);

            textViewJobitoTitle = (TextView) itemView.findViewById(R.id.textViewJobitoTitle);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            textViewDescription = (TextView) itemView.findViewById(R.id.textViewDescription);
            imageViewReviewerAvatar = (ImageView) itemView.findViewById(R.id.imageViewReviewerAvatar);
            textViewReviewerAndDate = (TextView) itemView.findViewById(R.id.textViewReviewerAndDate);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            openJobitoDetailsActivity();
        }

        private void openJobitoDetailsActivity() {

            Intent intent = new Intent(fragment.getActivity(), JobitoDetailsActivity.class);
            //intent.putExtra(AgendaFragment.EXTRA_PLAY_INDEX, playIndex);

            fragment.getActivity().startActivity(intent);

        }
    }
}
