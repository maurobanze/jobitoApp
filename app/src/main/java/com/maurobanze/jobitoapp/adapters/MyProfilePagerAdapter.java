package com.maurobanze.jobitoapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.fragments.MyAccountFragment;
import com.maurobanze.jobitoapp.fragments.MyReviewsFragment;
import com.maurobanze.jobitoapp.fragments.MySkillsFragment;

/**
 *
 */
public class MyProfilePagerAdapter extends FragmentStatePagerAdapter {

    private final Context context;


    public MyProfilePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case SECTION_MY_SKILLS:
                return new MySkillsFragment();

            case SECTION_REVIEWS:
                return new MyReviewsFragment();

            case SECTION_GENERAL_INFO:
                return new MyAccountFragment();

        }
        return null;

    }

    @Override
    public int getCount() {

        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {

            case SECTION_MY_SKILLS:
                return context.getString(R.string.my_profile_section_0).toUpperCase();

            case SECTION_REVIEWS:
                return context.getString(R.string.my_profile_section_1).toUpperCase();

            case SECTION_GENERAL_INFO:
                return context.getString(R.string.my_profile_section_2).toUpperCase();
        }

        return "fail";

    }

    private static final int SECTION_MY_SKILLS = 0;
    private static final int SECTION_REVIEWS = 1;
    private static final int SECTION_GENERAL_INFO = 2;

}
