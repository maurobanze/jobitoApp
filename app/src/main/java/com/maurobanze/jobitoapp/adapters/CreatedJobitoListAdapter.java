package com.maurobanze.jobitoapp.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maurobanze.jobitoapp.JobitoDetailsActivity;
import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.entities.Hire;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;

import java.util.ArrayList;

/**
 *
 */
public class CreatedJobitoListAdapter extends RecyclerView.Adapter<CreatedJobitoListAdapter.ViewHolder> {

    private Fragment fragment;
    private ArrayList<Job> jobs;

    public CreatedJobitoListAdapter(ArrayList<Job> jobs, Fragment fragment) {

        this.jobs = jobs;
        this.fragment = fragment;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.list_row_created_jobito, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Job job = jobs.get(position);

        holder.textViewTitle.setText(job.getTitle());
        holder.textViewDateCreated.setText(LocalDB.generateFakeDate(position));

        populateStatus(job.getStatus(), holder.textViewJobitoStatus);
        Hire currentHire = job.getCurrentHire();

        if (currentHire != null) {

            holder.textViewHiredWorkerName.setText(currentHire.getSelectedApplication().getApplicant().getName());

            switch (position) {
                case 0:
                    Glide.with(fragment).
                            load(R.drawable.avatar_girl).crossFade().centerCrop().into(holder.imageViewHiredWorkerAvatar);
                    break;
                case 2:
                    Glide.with(fragment).
                            load(R.drawable.avatar_guy_1).crossFade().centerCrop().into(holder.imageViewHiredWorkerAvatar);
                    break;
                case 3:
                    Glide.with(fragment).
                            load(R.drawable.avatar_girl_2).crossFade().centerCrop().into(holder.imageViewHiredWorkerAvatar);
                    break;

            }


            holder.textViewHiredWorkerName.setVisibility(View.VISIBLE);
            holder.imageViewHiredWorkerAvatar.setVisibility(View.VISIBLE);

        } else {

            holder.textViewHiredWorkerName.setVisibility(View.INVISIBLE);
            holder.imageViewHiredWorkerAvatar.setVisibility(View.INVISIBLE);
        }

        holder.jobIndex = position;
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    private void populateStatus(int status, TextView textViewStatus) {

        textViewStatus.setText(Job.getStatus(status));
        textViewStatus.setBackgroundColor(Job.getColorResource(fragment.getContext(), status));

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public int jobIndex;

        public TextView textViewTitle;
        public TextView textViewDateCreated;
        public TextView textViewHiredWorkerName;
        public TextView textViewJobitoStatus;
        public ImageView imageViewHiredWorkerAvatar;


        public ViewHolder(final View itemView) {
            super(itemView);

            textViewTitle = (TextView) itemView.findViewById(R.id.textViewJobitoTitle);
            textViewDateCreated = (TextView) itemView.findViewById(R.id.textViewDateCreated);
            textViewHiredWorkerName = (TextView) itemView.findViewById(R.id.textViewHiredWorker);
            textViewJobitoStatus = (TextView) itemView.findViewById(R.id.textViewStatus);

            imageViewHiredWorkerAvatar = (ImageView) itemView.findViewById(R.id.imageViewHiredWorkerAvatar);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            openJobitoDetailsActivity();
        }

        private void openJobitoDetailsActivity() {

            Intent intent = new Intent(fragment.getActivity(), JobitoDetailsActivity.class);
            //intent.putExtra(AgendaFragment.EXTRA_PLAY_INDEX, playIndex);

            fragment.getActivity().startActivity(intent);

        }
    }
}
