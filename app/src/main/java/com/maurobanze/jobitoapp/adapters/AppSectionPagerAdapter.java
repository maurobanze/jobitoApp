package com.maurobanze.jobitoapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.maurobanze.jobitoapp.MainActivity;
import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.fragments.AllJobitoFragment;
import com.maurobanze.jobitoapp.fragments.AppliedJobitoListFragment;
import com.maurobanze.jobitoapp.fragments.CreatedJobitoListFragment;
import com.maurobanze.jobitoapp.fragments.NewJobitoInfoFragment;
import com.maurobanze.jobitoapp.fragments.RecommendedJobitoListFragment;

/**
 *
 */
public class AppSectionPagerAdapter extends FragmentStatePagerAdapter {

    private final Context context;
    private int mode;


    public AppSectionPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        if (mode == MainActivity.MODE_AS_CLIENT) {

            if (position == SECTION_NEW_JOBITO ) {

                return new NewJobitoInfoFragment();

            } else if (position == SECTION_CREATED_JOBITOS){

                return new CreatedJobitoListFragment();
            }


        } else if (mode == MainActivity.MODE_AS_WORKER) {

            if (position == SECTION_RECOMMENDED_JOBITOS) {

                return new RecommendedJobitoListFragment();

            } else if (position == SECTION_APPLIED_JOBITOS) {

                return new AppliedJobitoListFragment();
            }

        } else if (mode == MainActivity.MODE_ALL_JOBITOS) {

            if (position == SECTION_ALL_JOBITOS) {

                return new AllJobitoFragment();
            }

        }

        return null;

    }

    @Override
    public int getCount() {

        if (mode == MainActivity.MODE_AS_CLIENT)
            return 2;

        if (mode == MainActivity.MODE_AS_WORKER)
            return 2;

        if (mode == MainActivity.MODE_ALL_JOBITOS)
            return 1;

        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (mode == MainActivity.MODE_AS_CLIENT) {

            if (position == SECTION_NEW_JOBITO ) {

                return context.getString(R.string.mode_client_section_0).toUpperCase();

            } else if (position == SECTION_CREATED_JOBITOS){

                return context.getString(R.string.mode_client_section_1).toUpperCase();

            }

            //add create jobito fragment. dnt forget to update getCount by this point

        } else if (mode == MainActivity.MODE_AS_WORKER) {

            if (position == SECTION_RECOMMENDED_JOBITOS) {

                return context.getString(R.string.mode_work_section_0).toUpperCase();


            } else if (position == SECTION_APPLIED_JOBITOS) {

                return context.getString(R.string.mode_work_section_1).toUpperCase();

            }

        } else if (mode == MainActivity.MODE_ALL_JOBITOS) {

            if (position == SECTION_ALL_JOBITOS)
                return context.getString(R.string.mode_all_jobito_section_0).toUpperCase();

        }

        return "fail";
    }

    @Override
    public int getItemPosition(Object object) {

        return POSITION_NONE;

    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {

        this.mode = mode;
    }

    private static final int SECTION_ALL_JOBITOS = 0;
    private static final int SECTION_RECOMMENDED_JOBITOS = 0;
    private static final int SECTION_CREATED_JOBITOS = 1;
    private static final int SECTION_APPLIED_JOBITOS = 1;
    private static final int SECTION_NEW_JOBITO = 0;
    //--- modes ----\\



}
