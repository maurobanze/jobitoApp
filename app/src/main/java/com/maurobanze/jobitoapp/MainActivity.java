package com.maurobanze.jobitoapp;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.maurobanze.jobitoapp.adapters.AppSectionPagerAdapter;
import com.maurobanze.jobitoapp.cloudApi.CloudConnector;
import com.maurobanze.jobitoapp.cloudApi.CloudError;
import com.maurobanze.jobitoapp.cloudApi.CloudResponse;
import com.maurobanze.jobitoapp.fragments.MySkillsFragment;

/**
 * Main Activity that contains the logic for the toolbar, the tab layout and the viewpager that is
 * glued to it and also the Navigation Drawer.
 * It is responsible for triggering fragment updates, header updates, tab updates and so on.
 */
public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TabLayout tabLayout;
    private AppSectionPagerAdapter pagerAdapter;
    private ViewPager viewPager;

    private MenuItem shouldUpdateToItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        updateToolbarTitle(R.string.title_main_activity_mode_client);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        pagerAdapter = new AppSectionPagerAdapter(getSupportFragmentManager(), this);
        pagerAdapter.setMode(MODE_AS_CLIENT);


        viewPager.setAdapter(pagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        initActionBarToggle();
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(navigationListener);
        navigationView.getHeaderView(0).setOnClickListener(clickListener);

        createFakeNotification();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_view_my_profile) {

            Intent intent = new Intent(this, MyProfileActivity.class);
            startActivity(intent);

        } else if (id == R.id.action_settings) {

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private NavigationView.OnNavigationItemSelectedListener navigationListener = new NavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {

            if (item.isChecked()) {
                drawerLayout.closeDrawers();
                return true;
            }
            if (item.getItemId() == R.id.navigate_settings) {

                openSettingsActivity();
                drawerLayout.closeDrawers();
                return true;
            }
            if (item.getItemId() == R.id.navigate_logout) {

                attemptLogoutUser();
                drawerLayout.closeDrawers();
                return true;
            }
            //item.setChecked(true);
            drawerLayout.closeDrawers();
            shouldUpdateToItem = item; //will trigger fragment update on DrawerListener onDrawerClosed method
            return true;
        }

    };

    private void initActionBarToggle() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.openDrawerDesc, R.string.closeDrawerDesc) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                if (shouldUpdateToItem != null) {

                    changeFragments(shouldUpdateToItem);
                    shouldUpdateToItem = null;

                }
            }

        };
    }


    private void changeFragments(MenuItem selectedItem) {

        switch (selectedItem.getItemId()) {
            case R.id.navigate_for_clients: {

                updateMode(MODE_AS_CLIENT);
                showTabLayoutIfNeeded();
                updateToolbarTitle(R.string.title_main_activity_mode_client);
                break;
            }
            case R.id.navigate_for_workers: {

                updateMode(MODE_AS_WORKER);
                showTabLayoutIfNeeded();
                updateToolbarTitle(R.string.title_main_activity_mode_worker);
                break;

            }
            case R.id.navigate_all_jobitos: {

                updateMode(MODE_ALL_JOBITOS);
                hideTabLayout();
                updateToolbarTitle(R.string.title_main_activity_mode_all_jobitos);
                break;

            }
        }

    }

    private void updateMode(int mode) {

        pagerAdapter.setMode(mode);
        pagerAdapter.notifyDataSetChanged();

        tabLayout.setupWithViewPager(viewPager);

    }

    private void hideTabLayout() {

        tabLayout.setVisibility(View.GONE);
    }

    public void showTabLayoutIfNeeded() {

        if (tabLayout.getVisibility() == View.GONE)
            tabLayout.setVisibility(View.VISIBLE);
    }

    private void updateToolbarTitle(@StringRes int resourceTitle) {

        toolbar.setTitle(resourceTitle);
    }

    private void openSettingsActivity() {

        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(MainActivity.this, MySkillsFragment.class);
            startActivity(intent);
        }
    };

    private void attemptLogoutUser() {

        CloudConnector cloudApi = JobitoApp.getCloudApiInstance();

        cloudApi.logoutUser(new CloudResponse<Void>() {

            @Override
            public void handleResponse(Void object) {

                finish();
                openLoginActivity();
            }

            @Override
            public void handleError(CloudError error) {
                Toast.makeText(MainActivity.this, error.getCode() + ": " + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private void openLoginActivity() {

        Intent intent = new Intent(this, LoginSignUpActivity.class);
        startActivity(intent);
    }

    private void createFakeNotification() {

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_girl);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(largeIcon)
                        .setContentTitle("Novo jobito disponível")
                        .setContentText("URGENTE. Meu laptop caiu e não liga mais. Não sei mais o que faço")
                        .setSubText("Informática -> Reparação");


        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, builder.build());

    }

    private void createFakeNotification2() {

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_guy_1);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(largeIcon)
                        .setContentTitle("Novo candidato")
                        .setContentText("Alfredo Muchanga candidatou-se ao seu jobito")
                        .setSubText("3 candidatos até agora")
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, builder.build());

    }

    public static final int MODE_AS_CLIENT = 1;
    public static final int MODE_AS_WORKER = 2;
    public static final int MODE_ALL_JOBITOS = 3;

}

