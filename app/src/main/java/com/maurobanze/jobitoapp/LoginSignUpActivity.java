package com.maurobanze.jobitoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.jobitoapp.cloudApi.CloudConnector;
import com.maurobanze.jobitoapp.cloudApi.CloudError;
import com.maurobanze.jobitoapp.cloudApi.CloudResponse;
import com.maurobanze.jobitoapp.entities.User;

public class LoginSignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private CloudConnector cloudApi;
    private EditText editTextCellphoneNumber;
    private MaterialDialog confirmCellphoneDialog;
    private MaterialDialog checkingCellphoneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fechtCloudApiInstance();

        if (cloudApi.isUserLoggedIn()) {
            openMainActivity();
            finish();

        } else {
            setContentView(R.layout.activity_login_sign_up);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            editTextCellphoneNumber = (EditText) findViewById(R.id.editTextCellphoneNumber);

            View buttonCreateAccount = findViewById(R.id.buttonCreateAccount);
            buttonCreateAccount.setOnClickListener(this);

            View buttonLogin = findViewById(R.id.buttonLogin);
            buttonLogin.setOnClickListener(this);

        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonCreateAccount) {

            try {

                openSignupActivity();
               /* NexmoApiClient smsClient = new NexmoApiClient(this);
                smsClient.sendMessage("+258824279642", "Codigo de registo: 1234", null);*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            //Intent intent = new Intent(LoginSignUpActivity.this, SignupFlowActivity.class);
            //startActivity(intent);

        } else if (v.getId() == R.id.buttonLogin) {

            String cellphone = editTextCellphoneNumber.getText().toString();

            if (isCellphoneNumberOk(cellphone)) {

                showConfirmCellphoneDialog(cellphone);

            }

        }

    }

    private void fechtCloudApiInstance() {

        cloudApi = JobitoApp.getCloudApiInstance();
    }

    private boolean isCellphoneNumberOk(String phoneNumber) {

        if (phoneNumber.isEmpty()) {

            toast(R.string.error_cellphone_empty);
            return false;

        } else if (phoneNumber.length() != 9) {

            toast(R.string.error_cellphone_invalid);
            return false;

        } else if ((!phoneNumber.startsWith("82") && (!phoneNumber.startsWith("84")) &&
                (!phoneNumber.startsWith("86")) && (!phoneNumber.startsWith("87")))) {

            toast(R.string.error_cellphone_invalid_prefix);
            return false;

        } else {

            return true;
        }

    }

    private void toast(@StringRes int stringResource) {

        Toast.makeText(this, stringResource, Toast.LENGTH_LONG).show();
    }

    private void showConfirmCellphoneDialog(final String cellphone) {
        confirmCellphoneDialog = new MaterialDialog.Builder(this)
                .content("Tem a certeza que o número <b>" + cellphone + "<b/> está correcto e pertence-lhe?")
                .cancelable(true)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .onPositive(confirmCellphoneCallback)
                .show();
    }

    private MaterialDialog.SingleButtonCallback confirmCellphoneCallback = new MaterialDialog.SingleButtonCallback() {

        @Override
        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

            showCheckingCellphoneDialog();
            checkExistsOnCloud(editTextCellphoneNumber.getText().toString());
        }
    };

    private void showCheckingCellphoneDialog() {
        checkingCellphoneDialog = new MaterialDialog.Builder(this)
                .title("Verificando o número")
                .content("Por favor aguarde...")
                .progress(true, 0)
                .cancelable(true)
                .show();
    }

    private void hideCheckingCellphoneDialog() {
        checkingCellphoneDialog.dismiss();
        checkingCellphoneDialog = null;

    }

    private void checkExistsOnCloud(String cellphone) {

        User user = new User(cellphone);

        fechtCloudApiInstance();
        cloudApi.login(user, new CloudResponse<User>() {

            @Override
            public void handleResponse(User user) {

                hideCheckingCellphoneDialog();
                openMainActivity();
                finish();
            }

            @Override
            public void handleError(CloudError error) {

                //cellphone number doesn't exist
                Toast.makeText(LoginSignUpActivity.this, "Não existe uma conta com este numero. Corrija-o ou uma nova conta" + error.getMessage(), Toast.LENGTH_LONG).show();
                hideCheckingCellphoneDialog();
            }
        });
    }

    private void openMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void openSignupActivity() {

        Intent intent = new Intent(this, SignupFlowActivity.class);
        startActivity(intent);
    }

}
