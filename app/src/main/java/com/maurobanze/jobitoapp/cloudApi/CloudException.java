package com.maurobanze.jobitoapp.cloudApi;

/**
 * Represents error for synchronous methods
 */
public class CloudException extends RuntimeException{

    private String code;
    private String message;
    private String details;

    public CloudException() {

    }

    public CloudException (String code, String message, String details, StackTraceElement[] stackTraceElement){

        this.code = code;
        this.message = message;
        this.details = details;
        setStackTrace(stackTraceElement);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
