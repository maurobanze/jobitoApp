package com.maurobanze.jobitoapp.cloudApi;

import android.content.Context;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.maurobanze.jobitoapp.entities.Province;
import com.maurobanze.jobitoapp.entities.User;

import java.util.List;

/**
 * Backendless Cloud Connector
 */
public class CloudApi implements CloudConnector {

    private static final String BACKENDLESS_APP_KEY = "AA54F352-41A5-5A4B-FFF6-ACC8EE468400";
    private static final String BACKENDLESS_ANDROID_SECRET = "6D0AD458-89F7-FDE7-FFC7-30B2769B0800";
    private static final String BACKENDLESS_APP_VERSION = "v1";

    private static final String DEFAULT_PASSWORD = "PASSWORD10091884";
    private Context context;

    public CloudApi(Context context) {

        this.context = context;
    }

    @Override
    public void initialize() {

        Backendless.initApp(context, BACKENDLESS_APP_KEY, BACKENDLESS_ANDROID_SECRET, BACKENDLESS_APP_VERSION);

    }


    @Override
    public void login(User user, final CloudResponse<User> callback) {

        Backendless.UserService.login(user.getCellphone(), DEFAULT_PASSWORD, new AsyncCallback<BackendlessUser>() {

            @Override
            public void handleResponse(BackendlessUser backendlessUser) {

                callback.handleResponse(null);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

                callback.handleError(errorFromBackendlessFault(backendlessFault));
            }
        }, true);

    }

    @Override
    public void registerUser(User user, final CloudResponse<User> callback) {

        BackendlessUser backendlessUser = new BackendlessUser();

        backendlessUser.setProperty(Schema.User.CELLPHONE, user.getCellphone());
        backendlessUser.setProperty(Schema.User.NAME, user.getName());
        backendlessUser.setPassword(DEFAULT_PASSWORD);
        backendlessUser.setProperty(Schema.User.BIRTH_DATE, user.getDateOfBirth());
        backendlessUser.setProperty(Schema.User.GENDER, user.getGender());

        Backendless.UserService.register(backendlessUser, new AsyncCallback<BackendlessUser>() {

                    @Override
                    public void handleResponse(BackendlessUser backendlessUser) {

                        callback.handleResponse(null);
                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {

                        callback.handleError(errorFromBackendlessFault(backendlessFault));
                    }
                }
        );

    }

    @Override
    public void fetchAllCities(final CloudResponse<List<Province>> callback) {

        Backendless.Persistence.of(Province.class).find(new AsyncCallback<BackendlessCollection<Province>>() {

            @Override
            public void handleResponse(BackendlessCollection<Province> cityCollection) {

                List<Province> cities = cityCollection.getCurrentPage();
                callback.handleResponse(cities);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

                callback.handleError(errorFromBackendlessFault(backendlessFault));
            }
        });

    }

    @Override
    public boolean isUserLoggedIn() {
        String userToken = UserTokenStorageFactory.instance().getStorage().get();

        if (userToken != null && !userToken.equals(""))
            return true;
        else
            return false;
    }


    @Override
    public void logoutUser(final CloudResponse<Void> callback) {

        Backendless.UserService.logout(new AsyncCallback<Void>() {

            @Override
            public void handleResponse(Void aVoid) {

                callback.handleResponse(null);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

                callback.handleError(errorFromBackendlessFault(backendlessFault));
            }
        });
    }

    /**
     * Converts Backendless error to our CloudApi exception, to help enforce loose-coupling
     */
    private CloudError errorFromBackendlessFault(BackendlessFault fault) {

        return new CloudError(fault.getCode(), fault.getMessage(), fault.getDetail());

    }

    /**
     * Converts Backendless Exception to our CloudApi exception, to help enforce loose-coupling
     */
    private CloudException exceptionFromBackendlessException(BackendlessException exception) {

        return new CloudException(exception.getCode(), exception.getMessage(),
                exception.getDetail(), exception.getStackTrace());

    }
}
