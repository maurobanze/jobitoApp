package com.maurobanze.jobitoapp.cloudApi;

/**
 * Definir callbacks para serem utilizados em chamadas asincronas
 */
public interface CloudResponse<T> {

    void handleResponse (T object);

    void handleError (CloudError error);//subject to changes

}
