package com.maurobanze.jobitoapp.cloudApi;

/**
 *
 */
public class Schema {

    public abstract class User{

        public static final String CELLPHONE = "cellphone";
        public static final String NAME = "name";
        public static final String BIRTH_DATE = "birthDate";
        public static final String GENDER = "gender";
        public static final String AVATAR_URL = "avatarUrl";

        public static final char GENDER_MASCULINE = 'm';
        public static final char GENDER_FEMININE = 'f';
    }
}
