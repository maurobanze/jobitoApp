package com.maurobanze.jobitoapp.cloudApi;

import com.maurobanze.jobitoapp.entities.Province;
import com.maurobanze.jobitoapp.entities.User;

import java.util.List;

/**
 * We would like to prevent marrying our app with a specific backend solution, because
 * we are not sure backendless will be sustainable in the long run and we want to write
 * world-class code.
 * This interface defines the interaction between the app and the cloud.
 * The app won't interact with Backendless directly, but instead will interact with this interface.
 * Thus, if we change backend, the app doesnt suffer. We just need to change implementations of this interface.
 *
 */
public interface CloudConnector {

    /**
     * Starts the connection with the Cloud backend
     */
    void initialize();

    void login(User user, CloudResponse<User> callback);

    void logoutUser(CloudResponse<Void> callback);

    boolean isUserLoggedIn ();

    void registerUser(User user, CloudResponse<User> callback);


    void fetchAllCities (CloudResponse<List<Province>> callback);



}
