package com.maurobanze.jobitoapp.cloudApi;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Escopo: Abstracao da comunicacao com o servico Nexmo, com o unico objectivo de
 * enviar SMS.
 */
public class NexmoApiClient {

    private final Context context;

    private static final String NEXMO_BASE_URL = "https://rest.nexmo.com/sms/json";
    private static final String MY_NEXMO_API_KEY = "8c6c5f24"; //on production, these can be decompiled and we get hacked
    private static final String MY_NEXMO_SECRET_KEY = "97f4d028a8fe20d9";

    private static final String JOBITO_SENDER_ID = "Jobito";

    public NexmoApiClient(Context context) {

        this.context = context;


    }

    public void sendMessage(String to, String message, CloudResponse<String> callback) {

        submitSmsSendRequest(to, JOBITO_SENDER_ID, message, callback);

    }

    //--- LOW LEVEL IMPLEMENTATION using Volley API ----

    /**
     * @param to
     * @param from
     * @param message
     * @param callback
     */

    private void submitSmsSendRequest(final String to, final String from,
                                      final String message, final CloudResponse<String> callback) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest sendSmsRequest = new StringRequest(Request.Method.POST, NEXMO_BASE_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(context, "Did it seeeeend???", Toast.LENGTH_SHORT).show();
                        Log.v("NexmoResponse", response);

                        //TODO: process respond using JSON parsing
                        //callback.handleResponse(response);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                //callback.handleError(new CloudError());

                Toast.makeText(context, "Volley Error", Toast.LENGTH_SHORT).show();
                Log.e("Volley error", error.networkResponse.statusCode + "");

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put(TO, to);
                params.put(FROM, from);
                params.put(TYPE, TYPE_TEXT);
                params.put(TEXT, message);
                params.put(API_KEY, MY_NEXMO_API_KEY);
                params.put(API_SECRET, MY_NEXMO_SECRET_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");

                return headers;
            }
        };


        requestQueue.add(sendSmsRequest);
    }

    private static final String API_KEY = "api_key";
    private static final String API_SECRET = "api_secret";
    private static final String TO = "to";
    private static final String FROM = "from";
    private static final String TYPE = "type";
    private static final String TYPE_TEXT = "text";
    private static final String TEXT = "text";


}
