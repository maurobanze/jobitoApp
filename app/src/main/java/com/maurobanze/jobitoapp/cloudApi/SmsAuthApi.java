package com.maurobanze.jobitoapp.cloudApi;

import android.content.Context;

/**
 * Authenticates users using 2 factor authentication (2FA) powered by Nexmo.
 * Uses Nexmo to send sms to cellphone number provided and then listen for incoming SMS's,
 * hoping to detect the expected message.
 * Users should only be able to try every 30 minutes (sms cost money), so this api should enforce it.
 *
 */
public class SmsAuthApi {

    private final Context context;
    private NexmoApiClient smsClient;
    private int timeout = 30000;//30 sec


    public SmsAuthApi(Context context) {

        this.context = context;
        smsClient = new NexmoApiClient(context);
    }

    public void aunthenticate (String cellphoneNumber){

        smsClient.sendMessage(cellphoneNumber, "Codigo", null);

    }

}
