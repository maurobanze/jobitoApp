package com.maurobanze.jobitoapp.entities;

import java.util.Date;

/**
 * Created by MauroBanze on 12/25/15.
 */
public class Comment {

    private int id;
    private String content;
    private Date datePublished;

    private User owner;

    public Comment() {
    }

    public Comment(int id, String content, Date datePublished, User owner) {
        this.id = id;
        this.content = content;
        this.datePublished = datePublished;
        this.owner = owner;
    }
}
