package com.maurobanze.jobitoapp.entities;

import java.util.Date;

/**
 * Created by MauroBanze on 5/24/16.
 */
public class UserSkill {

    private String id;
    private User user;
    private Skill skill;
    private Date dateAdded;

    public UserSkill() {
    }

    public UserSkill(String id, User user, Skill skill, Date dateAdded) {
        this.id = id;
        this.user = user;
        this.skill = skill;
        this.dateAdded = dateAdded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }
}
