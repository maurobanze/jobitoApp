package com.maurobanze.jobitoapp.entities;

import java.util.Date;

/**
 * Entity representing the application of a certain service provider to a certain jobito.
 *
 */
public class JobApplication {

    private int id;
    private Job job;
    private User applicant;
    private Date dateApplied;
    private String description;

    public JobApplication() {

    }

    public JobApplication(int id, Job job, User applicant, Date dateApplied, String description) {
        this.id = id;
        this.job = job;
        this.applicant = applicant;
        this.dateApplied = dateApplied;
        this.description = description;
    }

    public JobApplication(User applicant) {
        this.applicant = applicant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getApplicant() {
        return applicant;
    }

    public void setApplicant(User applicant) {
        this.applicant = applicant;
    }

    public Date getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(Date dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
