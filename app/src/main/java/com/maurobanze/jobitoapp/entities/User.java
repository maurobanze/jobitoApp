package com.maurobanze.jobitoapp.entities;

import java.util.Date;

/**
 * User of our service. Can be a Service Provider or a Client
 */
public class User {

    private int id;
    private String cellphone;
    private String password;
    private String avatarPath;
    private String name;
    private String aboutMe;
    private Date dateOfBirth;
    private char gender;

    private Province province;

    private double rating;


    public User() {

    }

    public User(String cellphone) {
        this.cellphone = cellphone;
    }

    public User(int id, String name, String avatarPath, double rating) {
        this.id = id;
        this.name = name;
        this.avatarPath = avatarPath;
        this.rating = rating;
    }

    public User(int id, String cellphone, String password, String avatarPath, String name,
                String aboutMe, Date dateOfBirth, char gender, Province province, double rating) {
        this.id = id;
        this.cellphone = cellphone;
        this.password = password;
        this.avatarPath = avatarPath;
        this.name = name;
        this.aboutMe = aboutMe;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.province = province;
        this.rating = rating;
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

}
