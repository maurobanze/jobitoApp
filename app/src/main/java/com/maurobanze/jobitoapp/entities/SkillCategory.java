package com.maurobanze.jobitoapp.entities;

import com.maurobanze.jobitoapp.interfaces.Listable;

import java.util.ArrayList;

/**
 *
 */
public class SkillCategory implements Listable{

    private int id;
    private String name;
    private String iconPath;

    private ArrayList<Skill> skills;

    public SkillCategory(String name) {
        this.name = name;
    }

    public SkillCategory(int id, String name, String iconPath) {
        this.id = id;
        this.name = name;
        this.iconPath = iconPath;
    }

    public SkillCategory(int id, String name, String iconPath, ArrayList<Skill> skills) {
        this.id = id;
        this.name = name;
        this.iconPath = iconPath;
        this.skills = skills;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getIconPath() {
        return iconPath;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String getItemTitle() {
        return name;
    }

    public String getItemIconPath() {
        return iconPath;
    }

    @Override
    public String getSecondaryText() {
        return null;
    }


}
