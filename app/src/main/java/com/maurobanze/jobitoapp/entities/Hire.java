package com.maurobanze.jobitoapp.entities;

import java.util.Date;

/**
 * A Hire is when a client selectes a worker to do a job.
 */
public class Hire {

    private String id;
    private Job job;
    private JobApplication selectedApplication;
    private Date hiringDate;

    private int rating;
    private String ratingComment;
    private Date ratingDate;

    /**
     * A user can cancel a hire because of bad service or the worker not showing up
     * So the status can be:
     */
    private int status;

    public Hire() {

    }

    public Hire(String id, Job job, JobApplication selectedApplication, Date hiringDate, int rating,
                String ratingComment, Date ratingDate, int status) {

        this.id = id;
        this.job = job;
        this.selectedApplication = selectedApplication;
        this.hiringDate = hiringDate;
        this.rating = rating;
        this.ratingComment = ratingComment;
        this.ratingDate = ratingDate;
        this.status = status;
    }

    public Hire(String id, int rating, String ratingComment, Date ratingDate) {
        this.id = id;
        this.rating = rating;
        this.ratingComment = ratingComment;
        this.ratingDate = ratingDate;
    }

    public Hire(JobApplication selectedApplication) {
        this.selectedApplication = selectedApplication;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public JobApplication getSelectedApplication() {
        return selectedApplication;
    }

    public void setSelectedApplication(JobApplication selectedApplication) {
        this.selectedApplication = selectedApplication;
    }

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRatingComment() {
        return ratingComment;
    }

    public void setRatingComment(String ratingComment) {
        this.ratingComment = ratingComment;
    }

    public Date getRatingDate() {
        return ratingDate;
    }

    public void setRatingDate(Date ratingDate) {
        this.ratingDate = ratingDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
