package com.maurobanze.jobitoapp.entities;

import com.maurobanze.jobitoapp.interfaces.Listable;

/**
 * Represents a skill or profession or trade of a Service Provider.
 * Every Service Provider has a set of skills, and Clients,when posting jobs, indicate
 * which skill they are looking for.
 * Each skill has a category. A particular "All from this category" skill object is included to
 * allow users to be general and indicate that they don't care for particular skills,
 * as long as it is included in a specific category
 *
 */
public class Skill implements Listable {

    private int id;
    private String name;
    private String description;
    private SkillCategory skillCategory;


    public Skill(int id, String name, String description, SkillCategory skillCategory) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.skillCategory = skillCategory;
    }

    public Skill(String name, SkillCategory skillCategory) {
        this.name = name;
        this.skillCategory = skillCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SkillCategory getSkillCategory() {
        return skillCategory;
    }

    public void setSkillCategory(SkillCategory skillCategory) {
        this.skillCategory = skillCategory;
    }

    @Override
    public String getItemTitle() {
        return name;
    }

    @Override
    public String getItemIconPath() {
        return skillCategory.getItemIconPath();
    }

    @Override
    public String getSecondaryText() {
        return null;
    }
}
