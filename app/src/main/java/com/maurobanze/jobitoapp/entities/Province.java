package com.maurobanze.jobitoapp.entities;

import android.location.Location;

import com.maurobanze.jobitoapp.interfaces.Listable;

/**
 * Represents cities, ex: Maputo, Tete, 'Qualquer provincia'
 */
public class Province implements Listable {

    private int id;
    private String name;

    //Geofence
    private Location centralPoint;
    private double radius;

    public Province() {}

    public Province(int id, String name, Location centralPoint, double radius) {
        this.id = id;
        this.name = name;
        this.centralPoint = centralPoint;
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getCentralPoint() {
        return centralPoint;
    }

    public void setCentralPoint(Location centralPoint) {
        this.centralPoint = centralPoint;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {

        return name;
    }

    @Override
    public String getItemTitle() {
        return name;
    }

    @Override
    public String getItemIconPath() {
        return "";
    }

    @Override
    public String getSecondaryText() {
        return "";
    }
}
