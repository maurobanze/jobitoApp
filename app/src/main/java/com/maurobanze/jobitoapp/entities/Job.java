package com.maurobanze.jobitoapp.entities;

import android.content.Context;

import com.maurobanze.jobitoapp.R;

import java.util.Date;

/**
 *
 */
public class Job {

    private int id;
    private User creator;
    private String title;
    private String description;
    private Skill skillNeeded;
    private Date dateCreated;
    private Province province;
    private double paymentOffered;

    /**
     * Chosen worker
     */
    private Hire currentHire;

    /**
     * Hires over time. Because user can re-open a jobito and select other users
     */
    private Hire[] pastHires;

    /**
     * Can be: OPEN,  WORKER_SELECTED_WORKING, CLOSED_NO_WORKER, CLOSED_WORKER_FINISHED, etc?,
     */
    private int status;


    public Job(String title) {

        this.title = title;
    }

    public Job(String title, Hire currentHire, int status) {
        this.title = title;
        this.currentHire = currentHire;
        this.status = status;
    }

    public Job(int id, String title, Skill skillNeeded, int status) {
        this.id = id;
        this.title = title;
        this.skillNeeded = skillNeeded;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Skill getSkillNeeded() {
        return skillNeeded;
    }

    public void setSkillNeeded(Skill skillNeeded) {
        this.skillNeeded = skillNeeded;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public double getPaymentOffered() {
        return paymentOffered;
    }

    public void setPaymentOffered(double paymentOffered) {
        this.paymentOffered = paymentOffered;
    }

    public Hire getCurrentHire() {
        return currentHire;
    }

    public void setCurrentHire(Hire currentHire) {
        this.currentHire = currentHire;
    }

    public Hire[] getPastHires() {
        return pastHires;
    }

    public void setPastHires(Hire[] pastHires) {
        this.pastHires = pastHires;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static final int STATUS_OPEN = 1;
    public static final int STATUS_WORKER_SELECTED_WORKING = 2;
    public static final int STATUS_CLOSED_WORKER_FINISHED = 3;
    public static final int STATUS_CLOSED_NO_WORKER = 4;

    public static String getStatus(int status) {

        switch (status) {

            case STATUS_OPEN:
                return "ABERTO";

            case STATUS_WORKER_SELECTED_WORKING:
                return "EM CURSO";

            case STATUS_CLOSED_WORKER_FINISHED:
                return "FECHADO";

            case STATUS_CLOSED_NO_WORKER:
                return "FECHADO";
        }

        return "fail";
    }

    public static int getColorResource(Context context, int status){

        switch (status) {

            case STATUS_OPEN:
                return context.getResources().getColor(R.color.jobito_status_open);

            case STATUS_WORKER_SELECTED_WORKING:
                return context.getResources().getColor(R.color.jobito_status_in_progress);

            case STATUS_CLOSED_WORKER_FINISHED:
                return context.getResources().getColor(R.color.jobito_status_closed);

            case STATUS_CLOSED_NO_WORKER:
                return context.getResources().getColor(R.color.jobito_status_closed);
        }

        return 0;
    }
}
