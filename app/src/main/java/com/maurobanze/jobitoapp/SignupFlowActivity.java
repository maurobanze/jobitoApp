package com.maurobanze.jobitoapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.jobitoapp.adapters.SignupPagerAdapter;
import com.maurobanze.jobitoapp.cloudApi.CloudConnector;
import com.maurobanze.jobitoapp.cloudApi.CloudError;
import com.maurobanze.jobitoapp.cloudApi.CloudResponse;
import com.maurobanze.jobitoapp.cloudApi.Schema;
import com.maurobanze.jobitoapp.entities.Province;
import com.maurobanze.jobitoapp.entities.User;
import com.maurobanze.jobitoapp.utils.ImagePicker;
import com.maurobanze.jobitoapp.views.ImprovedViewPager;
import com.soundcloud.android.crop.Crop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This activity represents the Signup Flow of the app.
 * It defines each screen in the signup process as a inner fragment.
 * These must be static per Framework design (Due to inner Fragments always holding outer Activity reference, which
 * causes memory leak after orientation changes (new Activity is created, but inner fragment points to old Activity
 * reference).
 *
 * There are Managing methods, responsible for managing all the steps (Fragments) and switching from one to the other.
 *
 */
public class SignupFlowActivity extends AppCompatActivity {
    private static Activity activity;

    private static User newUser;

    private static ImprovedViewPager viewPager;

    private static TextView textViewSignupProgress;

    private static ImagePicker imagePicker;
    private Bitmap pictureBitmap;
    private static MaterialDialog progressDialog;

    private boolean debugSkipSignup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_flow);

        activity = this;
        if (debugSkipSignup){

            openMainActivity();
            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_signup_flow);
        setSupportActionBar(toolbar);

        viewPager = (ImprovedViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new SignupPagerAdapter(getSupportFragmentManager(), this));
        viewPager.setPagingEnabled(false);

        textViewSignupProgress = (TextView) findViewById(R.id.textViewSignupProgress);


        newUser = new User();

    }

    /**
     * Step one of the Signup process is related to crucial account information like
     * username and passwords
     */
    public static class ScreenOne extends Fragment implements View.OnClickListener{

        private EditText editTextFullName;
        private EditText editTextCellphone;


        private Button buttonContinue;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            View v = inflater.inflate(R.layout.fragment_signup_step_one, null, false);

            editTextFullName = (EditText) v.findViewById(R.id.editTextFullName);
            editTextCellphone = (EditText) v.findViewById(R.id.editTextCellphoneNumber);

            buttonContinue = (Button) v.findViewById(R.id.buttonContinue);
            buttonContinue.setOnClickListener(this);


            return v;
        }

        @Override
        public void onClick(View v) {

            String fullName = editTextFullName.getText().toString();
            String cellPhone = editTextCellphone.getText().toString();

            boolean allOk = checkInputValidity(fullName, cellPhone);

            if (allOk){

                fillNewInfoOnUser(fullName, cellPhone);
                advanceToNextScreen();
            }

        }

        private boolean checkInputValidity(String fullName, String cellPhone) {

            if (!fullName.isEmpty()){

                if(!cellPhone.isEmpty()){

                    return true;
                }else{
                    Toast.makeText(getActivity(), "Introduza um numero de telefone valido", Toast.LENGTH_SHORT).show();

                }
            }else {

                Toast.makeText(getActivity(), "Preencha o seu Nome Completo", Toast.LENGTH_SHORT).show();
            }
            return false;
        }

        private void fillNewInfoOnUser (String fullName, String cellPhoneNumber){

            newUser.setName(fullName);
            newUser.setCellphone(cellPhoneNumber);
        }
    }

    public static class ScreenTwo extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

        private ImageView imageViewUserAvatar;
        private Button buttonPickUserAvatar;

        private EditText editTextBirthDate;
        private RadioButton radioButtonMasculine;
        private RadioButton radioButtonFeminine;

        private DatePickerDialog datePickerDialog;
        private long datePicked;

        private Spinner spinnerCity;
        private Button buttonContinue;

        public ScreenTwo() {

        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.fragment_signup_step_two, container, false);


            imageViewUserAvatar = (ImageView) v.findViewById(R.id.imageViewUserAvatar);
            buttonPickUserAvatar = (Button) v.findViewById(R.id.buttonPickUserAvatar);
            buttonPickUserAvatar.setOnClickListener(this);

            editTextBirthDate = (EditText) v.findViewById(R.id.editTextBirthDate);
            editTextBirthDate.setOnClickListener(this);

            datePickerDialog = new DatePickerDialog(getActivity(),this,1990,01,01);

            radioButtonMasculine = (RadioButton) v.findViewById(R.id.radioButtonSexMasculine);
            radioButtonFeminine = (RadioButton) v.findViewById(R.id.radioButtonSexFeminine);

            spinnerCity = (Spinner) v.findViewById(R.id.spinnerCities);

            buttonContinue = (Button) v.findViewById(R.id.buttonContinue);
            buttonContinue.setOnClickListener(this);

            prepareUI();

            return v;
        }

        private void prepareUI(){

            CloudConnector cloudApi = JobitoApp.getCloudApiInstance();
            cloudApi.fetchAllCities(new CloudResponse<List<Province>>() {

                @Override
                public void handleResponse(List<Province> cities) {

                    populateSpinnerCities(cities);
                }

                @Override
                public void handleError(CloudError error) {

                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }

        private void populateSpinnerCities(List<Province> cities){

            ArrayList<String> citiesString = new ArrayList<>();
            citiesString.add("Escolha uma cidade");

            for (int i = 0; i < cities.size(); i++) {

                citiesString.add(cities.get(i).getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                    android.R.layout.simple_spinner_item, citiesString);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCity.setAdapter(adapter);

        }

        @Override
        public void onClick(View v) {

            if (v == buttonPickUserAvatar){

                showTakePhotoOrGalleryDialog();

            }else if(v == editTextBirthDate){

                datePickerDialog.show();

            }else if (v == buttonContinue){

                boolean allOk = checkInputValidity();
                if (allOk){

                    fillNewInfoOnUser();
                    advanceToNextScreen();
                }

            }

        }

        private void fillNewInfoOnUser (){

            Date birthDate = new Date(datePicked);
            newUser.setDateOfBirth(birthDate);

            if (radioButtonMasculine.isChecked())
                newUser.setGender(Schema.User.GENDER_MASCULINE);
            else
                newUser.setGender(Schema.User.GENDER_FEMININE);



        }

        private boolean checkInputValidity (){

            return true;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            editTextBirthDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            datePicked = Date.UTC(year, monthOfYear, dayOfMonth, 1, 0, 0);

        }
    }

    //-------- Outer Managing methods --------//

    /**
     * Attempts to advance to the next screen on the signup process.
     * It calls checkErrorsAndWarn to check for user input validity
     * and either proceeds to the next screen or the MainActivity
     */
    private static void advanceToNextScreen(){

            int currentPage = viewPager.getCurrentItem();
            if (currentPage == STEP_TWO_AND_FINAL){//last step

                registerUser();


            }else {
                //advance to next page
                viewPager.setCurrentItem(currentPage + 1, true);
                textViewSignupProgress.setText("Passo " + (currentPage + 2) + " de 2");
            }

    }

    private static void showTakePhotoOrGalleryDialog(){

        imagePicker = new ImagePicker(activity);
        imagePicker.showTakePhotoOrGalleryDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.v("OnResult", "On Result chamado");
        if (resultCode == RESULT_OK) {

            if (requestCode == ImagePicker.REQUEST_CAMERA) {

                pictureBitmap = imagePicker.getPictureTaken();

            } else if (requestCode == ImagePicker.REQUEST_SELECT_FILE){

                Uri selectedImageUri = data.getData();
                Crop.of(selectedImageUri, null).asSquare().start(activity);

            } else if (requestCode == Crop.REQUEST_CROP){

                Toast.makeText(activity, "Cropping finished", Toast.LENGTH_SHORT).show();


            }
        }else{

            Log.v("OnResult", "result not ok");
        }
    }

    /**
     * Calls CloudAPI method to register user on cloud
     */
    private static void registerUser (){

        showSavingProgressDialog();

        CloudConnector cloudApi = JobitoApp.getCloudApiInstance();

        cloudApi.registerUser(newUser, new CloudResponse<User>() {

            @Override
            public void handleResponse(User object) {

                hideProgressDialog();
                openMainActivity();
                Toast.makeText(activity, "Utilizador registado com sucesso", Toast.LENGTH_LONG).show();

            }

            @Override
            public void handleError(CloudError error) {

                hideProgressDialog();
                showErrorSavingDialog();
            }
        });
    }
    /**
     * Displays a progress dialog for when saving user on cloud
     */
    private static void showSavingProgressDialog(){
        progressDialog = new MaterialDialog.Builder(activity)
                .title("Salvando...")
                .content("Por favor aguarde")
                .progress(true, 0)
                .cancelable(true)
                .show();
    }

    private static void showErrorSavingDialog(){

        progressDialog = new MaterialDialog.Builder(activity)
                .title("Erro!!!")
                .content("Verifique a sua conexao a Internet e tente novamente")
                .cancelable(true)
                .positiveText("OK")
                .show();
    }

    private static void hideProgressDialog (){

        if (progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }

    }

    @Override
    public void onBackPressed() {

        int currentPage = viewPager.getCurrentItem();

        if (currentPage == 0){
            super.onBackPressed();

        } else{

            viewPager.setCurrentItem(currentPage - 1, true);
            textViewSignupProgress.setText("Passo " + (currentPage)+ " de 2");
        }
    }

    private static void openMainActivity(){


        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        Toast.makeText(activity, "Parabens, registado com sucesso", Toast.LENGTH_LONG).show();

        activity.finish();

    }

    private static final int STEP_ONE = 0;
    private static final int STEP_TWO_AND_FINAL = 1;

}
