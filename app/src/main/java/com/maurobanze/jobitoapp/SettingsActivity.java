package com.maurobanze.jobitoapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import de.psdev.licensesdialog.LicensesDialog;

//TODO: Add Notifications related settings (Notification frequency, types of notifications (new jobitos, client checked my profile),
public class SettingsActivity extends AppCompatActivity {

    private TextView textViewMainScreen;
    private MaterialDialog mainScreenDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(backListener);

        findViewById(R.id.layoutMainScreen).setOnClickListener(clickListener);
        findViewById(R.id.layoutSendFeedback).setOnClickListener(clickListener);
        findViewById(R.id.layoutAppVersion).setOnClickListener(clickListener);
        findViewById(R.id.layoutOpenSourceLicences).setOnClickListener(clickListener);

        textViewMainScreen = (TextView) findViewById(R.id.textViewMainScreen);

    }

    private View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.layoutMainScreen: {

                    showPickMainScreenDialog();
                    break;
                }
                case R.id.layoutSendFeedback: {

                    showSendEmailChooser();
                    break;
                }
                case R.id.layoutAppVersion: {

                    break;
                }
                case R.id.layoutOpenSourceLicences: {
                    showOpenSourceLicencesDialog();
                    break;
                }
            }

        }
    };

    private void showPickMainScreenDialog() {

        mainScreenDialog = new MaterialDialog.Builder(this)
                .title("Escolha o ecrã Principal")
                .negativeText(R.string.cancel)
                .items(R.array.app_areas)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {

                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                        saveMainScreen(text);
                        return true;
                    }
                })
                .show();
    }


    private void saveMainScreen(CharSequence screen) {

        textViewMainScreen.setText(screen);
    }

    private void showSendEmailChooser() {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:maurobanze@gmail.com"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");

        startActivity(Intent.createChooser(emailIntent, "Enviar feedback usando..."));
    }

    private void showOpenSourceLicencesDialog() {

        new LicensesDialog.Builder(this)
                .setNotices(R.raw.open_source_licences)
                .setTitle("Licensas Open Source")
                .setCloseText(R.string.cancel)
                .build()
                .show();

    }

    private static final int SETTING_MAIN_SCREEN_CLIENT = 0;
    private static final int SETTING_MAIN_SCREEN_WORKER = 1;

}
