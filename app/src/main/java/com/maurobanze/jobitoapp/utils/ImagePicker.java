package com.maurobanze.jobitoapp.utils;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.jobitoapp.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by Mauro Banze
 * This class encapsulates the functionality needed to allow the user to take a picture
 * using the built in camera or choose an existing photo from the Gallery.
 * It was designed to be used with an Activity
 */
public class ImagePicker {

    /**
     * The activity that this component will exist on.
     * The activity that will be called to return the image fetched by this component
     */
    private Activity activity;

    /**
     * Constants used to store the option the user chooses (Take picture or select existing)
     */
    public static final int REQUEST_CAMERA = 0;
    public static final int REQUEST_SELECT_FILE = 1;

    /**
     * The width and height of the image.
     * We should later discuss what the best size is taking quality and bandwidth usage into account
     */
    private static final int IMAGE_HEIGHT = 800;
    private static final int IMAGE_WIDTH = 800;

    /**
     * A filename used by the built-in camera to store the image temporarly
     */
    private static final String TEMP_FILE = "temp.jpg";

    /**
     * File used to store the path to the image temporarly
     */
    private File f;

    private File imageFile;

    public Uri finalImageUri;

    public ImagePicker(Activity activity) {

        this.activity = activity;
    }

    public void showTakePhotoOrGalleryDialog(){
        new MaterialDialog.Builder(activity)
                .title(R.string.add_photo)
                .negativeText(R.string.cancel)
                .items(R.array.get_photo_options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int item, CharSequence text) {

                        if (item == 0) {//Take picture

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File f = new File(android.os.Environment
                                    .getExternalStorageDirectory(), TEMP_FILE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                            activity.startActivityForResult(intent, REQUEST_CAMERA);

                        } else if (item == 1) {//Select from gallery

                            Crop.pickImage(activity, REQUEST_SELECT_FILE );

                        }
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNegative(MaterialDialog dialog) {

                        dialog.dismiss();
                    }
                })
                .show();

    }

    /**
     *Called only when the user chose to take a picture.
     * Returns the picture taken
     */
    public Bitmap getPictureTaken (){

        f = new File(Environment.getExternalStorageDirectory()
                .toString());
        for (File temp : f.listFiles()) {
            if (temp.getName().equals(TEMP_FILE)) {

                f = temp;
                imageFile = temp;
                break;
            }
        }
        try {
            if (f == null){
                Toast.makeText(activity,"temp is null",Toast.LENGTH_SHORT).show();
            }
            Bitmap bm;
            BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

            bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
                    btmapOptions);

            bm = Bitmap.createScaledBitmap(bm, IMAGE_WIDTH, IMAGE_HEIGHT, true);
            return bm;

        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     *Fetches the image that was chosen by the user
     * Needs Intent to get the url of the file
     */
    public Bitmap getChosenPicture(Intent data){

        Uri selectedImageUri = data.getData();
        String tempPath = getPath(selectedImageUri);

        imageFile = new File(tempPath);

        Bitmap bm;
        BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
        bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
        bm = Bitmap.createScaledBitmap(bm, IMAGE_WIDTH, IMAGE_HEIGHT, true);

        return bm;
    }

    /**
     *
     * Determines the path of the chosen image
     */
    private String getPath(Uri uri) {

        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();


        return cursor.getString(column_index);
    }


    public File savePhoto(Bitmap bmp) throws IOException {

        File imageFileFolder = new File(
                Environment.getExternalStorageDirectory(), "/Pictures/OurMoz");

        imageFileFolder.mkdir();
        FileOutputStream out = null;

        Calendar c = Calendar.getInstance();
        String date = fromInt(c.get(Calendar.MONTH))
                + fromInt(c.get(Calendar.DAY_OF_MONTH))
                + fromInt(c.get(Calendar.YEAR))
                + fromInt(c.get(Calendar.HOUR_OF_DAY))
                + fromInt(c.get(Calendar.MINUTE))
                + fromInt(c.get(Calendar.SECOND));

        File imageFileName = new File(imageFileFolder, date + ".png");

        out = new FileOutputStream(imageFileName);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        out.flush();
        out.close();

        out = null;

        return imageFileName;
    }

    public String fromInt(int val){

        return String.valueOf(val);
    }

    public File getImageFile(){

        return imageFile;
    }

}
