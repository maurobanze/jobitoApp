package com.maurobanze.jobitoapp;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.jobitoapp.entities.Comment;
import com.maurobanze.jobitoapp.entities.JobApplication;
import com.maurobanze.jobitoapp.entities.User;

import java.util.ArrayList;
import java.util.Date;

public class JobitoDetailsActivity extends AppCompatActivity {

    private ViewGroup layoutApplicants,
            layoutComments;
    private TextView textViewApplicants, textViewComments;

    private ArrayList<JobApplication> jobApplications;
    private ArrayList<Comment> comments;

    private Button buttonTakeAction;
    private int ownership;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobito_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(backListener);

        textViewApplicants = (TextView) findViewById(R.id.textViewApplicants);
        textViewComments = (TextView) findViewById(R.id.textViewComments);
        layoutApplicants = (ViewGroup) findViewById(R.id.layoutApplicants);
        layoutComments = (ViewGroup) findViewById(R.id.layoutComments);

        buttonTakeAction = (Button) findViewById(R.id.buttonTakeAction);

        activateMode(OWNERSHIP_OWNER, MODE_OPEN);
        loadApplicants();
        //loadComments();
        drawApplicantsOnScreen();
        drawCommentsOnScreen();
    }

    private void loadApplicants() {

        JobApplication j1 = new JobApplication(1, null, new User(2, "Felix Barros", "imageURL", 5),
                new Date(), "Tenho experiencia em design de websites. Veja o meu perfil para ter uma ideia.");

        JobApplication j2 = new JobApplication(2, null, new User(9, "Alfredo Muchanga", "imageURL", 3.5),
                new Date(), "Trabalho como programador ha mais de 4 anos, tendo desenvolvido varios websites.");

        JobApplication j3 = new JobApplication(3, null, new User(5, "Salima Sitoe", "imageURL", 4),
                new Date(), "Tenho disponibilidade para comecar a trabalhar imediatamente neste jobito. Escolha-me.");

        jobApplications = new ArrayList<>();
        jobApplications.add(j1);
        jobApplications.add(j2);
        jobApplications.add(j3);

    }

    private void loadComments() {

        Comment c1 = new Comment(1, "Por favor seja mais especifico quanto a aquilo que precisa, pra melhor avaliarmos a " +
                "oportunidade.",
                new Date(), new User(1, "Alfredo Muchanga", "imageURL", 4));

        comments = new ArrayList<>();
        comments.add(c1);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void drawApplicantsOnScreen() {

        layoutApplicants.removeAllViews();
        textViewApplicants.setText(getString(R.string.applicants) + " (" + jobApplications.size() + ")");

        for (int i = 0; i < jobApplications.size(); i++) {

            JobApplication jobApplication = jobApplications.get(i);

            if ((mode == MODE_WORKER_CHOOSEN_FINISHED) && (i == 0)) {

                View row = LayoutInflater.from(this).inflate(R.layout.jobito_applicant_chosen_row, layoutApplicants, false);
                TextView textViewStatus = (TextView) row.findViewById(R.id.textViewStatus);
                TextView textViewApplicantName = (TextView) row.findViewById(R.id.textViewApplicantName);

                textViewApplicantName.setText(jobApplication.getApplicant().getName());
                textViewStatus.setText("AVALIADO");
                textViewStatus.setBackgroundColor(getResources().getColor(R.color.jobito_status_open));

                row.setTag(jobApplication.getId());//for onClick identification
                row.setOnClickListener(rowListener);

                layoutApplicants.addView(row);
                continue;
            }

            if ((mode == MODE_WORKER_CHOOSEN_WORKING) && (i == 0)) {

                View row = LayoutInflater.from(this).inflate(R.layout.jobito_applicant_chosen_row, layoutApplicants, false);
                TextView textViewStatus = (TextView) row.findViewById(R.id.textViewStatus);
                TextView textViewApplicantName = (TextView) row.findViewById(R.id.textViewApplicantName);
                TextView textViewRating = (TextView) row.findViewById(R.id.textViewRating);


                textViewApplicantName.setText(jobApplication.getApplicant().getName());
                textViewRating.setCompoundDrawables(null, null, null, null);
                textViewRating.setText("AVALIAR");
                textViewRating.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                textViewRating.setTextSize(19);

                row.setTag(jobApplication.getId());//for onClick identification
                row.setOnClickListener(rowListener);

                layoutApplicants.addView(row);
                continue;
            }

            View row = LayoutInflater.from(this).inflate(R.layout.jobito_applicant_row, layoutApplicants, false);

            TextView textViewApplicantName = (TextView) row.findViewById(R.id.textViewApplicantName);
            TextView textViewApplicantDescription = (TextView) row.findViewById(R.id.textViewApplicantDescription);
            RatingBar ratingBar = (RatingBar) row.findViewById(R.id.ratingBar);

            textViewApplicantName.setText(jobApplication.getApplicant().getName());
            ratingBar.setRating((float) jobApplication.getApplicant().getRating());
            textViewApplicantDescription.setText(jobApplication.getDescription());

            row.setTag(jobApplication.getId());//for onClick identification

            row.setOnClickListener(rowListener);

            layoutApplicants.addView(row);

        }


    }

    private View.OnClickListener rowListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(JobitoDetailsActivity.this, ApplicantProfileActivity.class);
            startActivityForResult(intent, 0);//TODO: review

        }
    };

    private void drawCommentsOnScreen() {


    }

    private void showCreateCommentDialog() {

        new MaterialDialog.Builder(this)
                .title(R.string.dialog_new_comment_title)
                .customView(R.layout.dialog_new_comment, true)
                .positiveText(R.string.dialog_new_comment_submit)
                .negativeText(R.string.cancel)
                .show();
    }

    private void activateMode(int ownership, int mode) {

        this.ownership = ownership;
        this.mode = mode;

        if (ownership == OWNERSHIP_OWNER) {

            if (mode == MODE_OPEN) {

                buttonTakeAction.setEnabled(false);
                buttonTakeAction.setBackgroundColor(getResources().getColor(R.color.disabled_button));
                buttonTakeAction.setTextColor(getResources().getColor(R.color.disabled_button_text));
                buttonTakeAction.setText("Escolha um candidato");


            } else if (mode == MODE_WORKER_CHOOSEN_WORKING) {

                buttonTakeAction.setEnabled(true);
                buttonTakeAction.setText("Avaliar o escolhido");

            } else if (mode == MODE_WORKER_CHOOSEN_FINISHED) {

                buttonTakeAction.setVisibility(View.GONE);
                buttonTakeAction.setVisibility(View.GONE);

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_jobito_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_comment) {

            showCreateCommentDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private static final int OWNERSHIP_CANDIDATE = 0;
    private static final int OWNERSHIP_OWNER = 1;

    private static final int MODE_OPEN = 0;
    private static final int MODE_WORKER_CHOOSEN_WORKING = 1;
    private static final int MODE_WORKER_CHOOSEN_FINISHED = 2; // Job ONLY FINISHES WHEN worker is rated


}
