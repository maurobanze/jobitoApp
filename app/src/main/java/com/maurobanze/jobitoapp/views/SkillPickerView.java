package com.maurobanze.jobitoapp.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.adapters.ListableListAdapter;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.interfaces.Listable;
import com.maurobanze.jobitoapp.interfaces.ListableItemHolder;

import java.util.ArrayList;


/**
 * Component allows for the choosing of a skill. It first displays the categories,
 * then the skills within the chosen category. Then users can select a skill.
 * After that, this view has the option to show a user-provided view underneath the top navigation bar.
 * If not set, this view will only notify it's user of skill selection.
 * It requires that a listener is provided so that it can inform when users make selections
 */
public class SkillPickerView extends LinearLayout implements ListableItemHolder {

    private ArrayList<SkillCategory> categories;

    private SkillCategory selectedCategory;
    private Skill selectedSkill;

    private TextView textViewBreadcumb;
    private Button buttonBack;

    private RecyclerView recyclerView;
    private View selectedSkillView;


    private SelectionListener selectionListener;

    public SkillPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeViews(context);

    }

    private void initializeViews(Context context) {

        LayoutInflater.from(context).
                inflate(R.layout.view_skill_picker, this, true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        textViewBreadcumb = (TextView) findViewById(R.id.textViewBreadcumb);
        buttonBack = (Button) findViewById(R.id.buttonNavigationBack);
        buttonBack.setOnClickListener(listenerBack);
    }


    //     ***Getters and Setters***     \\

    public void setCategories(ArrayList<SkillCategory> categories) {
        this.categories = categories;
        updateItemsOnScreen(categories);
    }


    public void setSelectionListener(SelectionListener listener) {
        this.selectionListener = listener;
    }


    //     -******-

    private void updateItemsOnScreen(ArrayList<? extends Listable> items) {

        ListableListAdapter adapter = new ListableListAdapter(items, getContext(), this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(Listable item, View itemView, int itemPosition) {

        if (selectionListener == null) {

            toastMsg("Error SkillPickerView listener wasn't set. You forgot?");
            return;
        }

        if (item instanceof SkillCategory) {

            selectedCategory = (SkillCategory) item;
            updateItemsOnScreen(selectedCategory.getSkills());
            selectionListener.onUserSelected(selectedCategory, null);

            buttonBack.setVisibility(VISIBLE);


        } else if (item instanceof Skill) {

            selectedSkill = (Skill) item;

            selectionListener.onUserSelected(null, selectedSkill);

            if(selectedSkillView != null)
                // must be after selectionListenerCall. Works for now, but possibly brakes if we introduce asynchronous
                swapSelectedSkillView();
        }

        updateBreadcumb();
    }

    public View.OnClickListener listenerBack = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            goBack();
            updateBreadcumb();
        }
    };

    public void goBack() {

        if (isRootLevel()) {//Can't go back any further
            return;
        }

        if (selectedSkill != null) {
            // last level of hierarchy, skill had been selected. Unselect it

            selectedSkill = null;

            if (selectedSkillView != null)
                swapSelectedSkillView();

        } else {
            //unselect category
            selectedCategory = null;
            updateItemsOnScreen(categories);

            buttonBack.setVisibility(View.INVISIBLE);
        }

    }

    public void setSelectedSkillView(View selectedSkillView){

        this.selectedSkillView = selectedSkillView;

    }

    private void swapSelectedSkillView(){

        if (selectedSkillViewShowing()){

            removeView(selectedSkillView);
            recyclerView.setVisibility(View.VISIBLE);
        }else{

            recyclerView.setVisibility(View.GONE);
            addView(selectedSkillView);
        }

    }

    private boolean selectedSkillViewShowing(){

        return recyclerView.getVisibility() == View.GONE;
        //return selectedSkillView.getParent() == this;
    }

    public boolean isRootLevel() {

        /*
         * Selected category is only != null when displaying skills. If it is null, it means no category has been selected
         * which means that we are still at showing-categories level, which is root level.
         */
        if (selectedCategory == null) {
            return true;
        } else {
            return false;
        }
    }

    private void toastMsg(String msg) {

        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    private void updateBreadcumb(){

        textViewBreadcumb.setText(getBreadcumbText());
    }

    /**
     * Shows current navigation level as a text.
     * Ex: "Todas Categorias -> Trabalhos Domesticos"
     */
    public String getBreadcumbText() {

        if (isRootLevel()) {

            return getContext().getString(R.string.skill_categories_all);
        }

        if (selectedSkill != null) {
            return selectedCategory.getName() + " > " + selectedSkill.getName();
        } else {
            return selectedCategory.getName();
        }

    }

    public interface SelectionListener {

        void onUserSelected(@Nullable SkillCategory category, @Nullable Skill skill);


    }

}
