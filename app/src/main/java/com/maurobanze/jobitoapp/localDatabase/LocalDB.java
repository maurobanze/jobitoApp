package com.maurobanze.jobitoapp.localDatabase;

import com.maurobanze.jobitoapp.entities.Hire;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.entities.JobApplication;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.entities.User;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
public class LocalDB {


    public ArrayList<SkillCategory> fetchSkillCategories() {

        ArrayList<SkillCategory> categories = new ArrayList<>();


        categories.add(new SkillCategory(1, "Trabalhos Domeésticos", "icon"));
        categories.add(new SkillCategory(2, "Eventos e Festas", "icon"));
        categories.add(new SkillCategory(3, "Informatica", "icon"));
        categories.add(new SkillCategory(4, "Beleza, Moda e Modelagem", "icon"));
        categories.add(new SkillCategory(5, "Informatica", "icon"));
        categories.add(new SkillCategory(6, "Informatica", "icon"));

        return categories;

    }


    public ArrayList<SkillCategory> fetchSkillCategoriesWithSkills() {

        ArrayList<SkillCategory> categories = new ArrayList<>();

        SkillCategory fake = null;

        SkillCategory cat1 = new SkillCategory(1, "Trabalhos Domésticos", "ic_action_home");
        SkillCategory cat2 = new SkillCategory(2, "Eventos e Festas", "ic_action_event");
        SkillCategory cat3 = new SkillCategory(3, "Informática", "ic_hardware_computer");
        SkillCategory cat4 = new SkillCategory(4, "Beleza, Moda e Modelagem", "ic_action_beauty");

        cat1.setSkills(fetchSkills(cat1));
        cat2.setSkills(fetchSkills(cat2));
        cat3.setSkills(fetchSkills(cat3));
        cat4.setSkills(fetchSkills(cat4));


        categories.add(cat1);
        categories.add(cat2);
        categories.add(cat3);
        categories.add(cat4);

        return categories;

    }


    public ArrayList<Skill> fetchSkills(SkillCategory category) {

        ArrayList<Skill> skills = new ArrayList<>();

        skills.add(new Skill(1, "Canalizador", "Concebe e desenvolve websites", category));
        skills.add(new Skill(2, "Carpinteiro", "Concebe e desenvolve aplicações e sistemas", category));
        skills.add(new Skill(2, "Electrecista", "Repara avarias no hardware de um computador", category));
        skills.add(new Skill(2, "Pedreiro", "Concebe e desenvolve aplicações e sistemas", category));
        skills.add(new Skill(2, "Pintor", "Repara avarias no hardware de um computador", category));
        skills.add(new Skill(1, "Reparador de electrodomésticos", "Concebe e desenvolve websites", category));
        skills.add(new Skill(1, "Serralheiro", "Concebe e desenvolve websites", category));

        return skills;
    }

    public ArrayList<Skill> fetchSkills(User user) {

        ArrayList<Skill> skills = new ArrayList<>();

        skills.add(new Skill(1, "Designer Web", "Concebe e desenvolve websites", null));
        skills.add(new Skill(2, "App Developer", "Concebe e desenvolve aplicações e sistemas", null));
        skills.add(new Skill(2, "Reparador de Hardware", "Repara avarias no hardware de um computador", null));

        return skills;
    }

    public Skill getSkill(int id) {

        return new Skill(2, "Reparador de Hardware", "Repara avarias no hardware de um computador", null);

    }


    public ArrayList<Hire> createFakeHires() {

        ArrayList<Job> myJobs = createFakeJobs();

        Hire h0 = new Hire("1", 5, "Gostei muito do profissionalismo dele. Chegou a tempo e horas, é comunicativo e no " +
                "final resolveu-me o problema ", new Date());
        h0.setJob(myJobs.get(0));

        Hire h1 = new Hire("2", 4, "Recomendo este trabalhador. Estava mesmo à rasca e após entrar em contacto com ele, ele foi  " +
                "muito dinamico e nos encontramos o mais rápido possível. Acabou saindo mais caro do que eu imaginava, mas " +
                "estou satisfeiro ", new Date());
        h1.setJob(myJobs.get(1));

        Hire h2 = new Hire("1", 4, "Gostei muito do profissionalismo dele. Chegou a tempo e horas, é comunicativo e " +
                "até instalou outros programas úteis. Recomendo imenso.", new Date());
        h1.setJob(myJobs.get(2));

        ArrayList<Hire> hires = new ArrayList<>();
        hires.add(h0);
        hires.add(h1);
        hires.add(h2);

        return hires;

    }

    public ArrayList<Job> createFakeJobs() {

        Job j0 = new Job("Criação de um blog sobre a gastronomia moçambicana");
        Job j1 = new Job("URGENTE. Meu laptop caiu e não liga mais.");
        Job j2 = new Job("Preciso de alguém para colocar Anti-Virus e Microsoft Word no meu computador");
        Job j3 = new Job("Precisamos de um informático capaz de fazer um website para a nossa loja de sapatos");


        ArrayList jobs = new ArrayList();
        jobs.add(j0);
        jobs.add(j1);
        jobs.add(j2);
        jobs.add(j3);

        return jobs;

    }

    /**
     * Jobs I created
     *
     * @return
     */
    public static ArrayList<Job> generateFakeJobsCreated() {

        Job j0 = new Job("Tenho um aniversário e preciso de bolo para 20 pessoas",
                new Hire(new JobApplication(new User(2, "Alice Jorge"))), Job.STATUS_WORKER_SELECTED_WORKING);

        Job j1 = new Job("Preciso de um carpinteiro para fazer-me uma estante e cabeceira", null, Job.STATUS_OPEN);
        Job j2 = new Job("Preciso de alguém para fazer salgados e sobremesas para o um convivio",
                new Hire(new JobApplication(new User(2, "Félix Barros"))), Job.STATUS_CLOSED_WORKER_FINISHED);

        Job j3 = new Job("Quero um pintor para tratar de uma casa tipo 3. URGENTE",
                new Hire(new JobApplication(new User(2, "Maria Socas"))), Job.STATUS_CLOSED_WORKER_FINISHED);


        ArrayList jobs = new ArrayList();
        jobs.add(j0);
        jobs.add(j1);
        jobs.add(j2);
        jobs.add(j3);

        return jobs;

    }

    public static String generateFakeDate(int position) {

        ArrayList<String> dates = new ArrayList<>();
        dates.add("há 3h");
        dates.add("ontem");
        dates.add("há 1 sem");
        dates.add("há 2 meses");


        return dates.get(position);
    }

    public static ArrayList<Job> generateFakeJobsApplied() {

        SkillCategory skillInformatica = new SkillCategory("Informática");
        Job j0 = new Job(1, "Criação de um blog sobre a gastronomia moçambicana",
                new Skill("Web Design", skillInformatica), Job.STATUS_OPEN);

        Job j1 = new Job(2, "URGENTE. Meu laptop caiu e não liga mais.",
                new Skill("Reparação", skillInformatica), Job.STATUS_OPEN);

        Job j2 = new Job(3, "Preciso de alguém para colocar Anti-Virus e Microsoft Word no meu computador",
                new Skill("Instalação", skillInformatica), Job.STATUS_OPEN);

        Job j3 = new Job(4, "O Windows não inicia após fazer alguma actualização. Alguém pode me ajudar?",
                new Skill("Reparação", skillInformatica), Job.STATUS_CLOSED_WORKER_FINISHED);

        Job j4 = new Job(4, "Precisamos de um informático capaz de fazer um website para a nossa loja de sapatos",
                new Skill("Web Design", skillInformatica), Job.STATUS_CLOSED_WORKER_FINISHED);


        ArrayList<Job> jobs = new ArrayList<>();
        jobs.add(j0);
        jobs.add(j1);
        jobs.add(j2);
        jobs.add(j3);
        jobs.add(j4);

        return jobs;

    }

    public static String generateFakeDateApplied(int position) {

        ArrayList<String> dates = new ArrayList<>();
        dates.add("há 1 min");
        dates.add("há 4h");
        dates.add("há 20h");
        dates.add("há 1 sem");
        dates.add("há 1 mês");


        return dates.get(position);
    }

    public static ArrayList<Job> generateFakeJobsRecommended() {

        SkillCategory skillInformatica = new SkillCategory("Informática");

        Job j0 = new Job(1, "O meu celular não faz chamadas após entrar na água. Ajuda por favor",
                new Skill("Reparação", skillInformatica), Job.STATUS_OPEN);

        Job j1 = new Job(2, "Criação de um blog sobre a gastronomia moçambicana",
                new Skill("Web Design", skillInformatica), Job.STATUS_OPEN);

        Job j2 = new Job(3, "Preciso de um site para a minha empresa bem como um email do tipo xyz@minhaempresa.co.mz",
                new Skill("Web Design", skillInformatica), Job.STATUS_OPEN);

        Job j3 = new Job(4, "URGENTE. Meu laptop caiu e não liga mais.",
                new Skill("Reparação", skillInformatica), Job.STATUS_OPEN);

        Job j4 = new Job(5, "Preciso de alguém para colocar Anti-Virus e Microsoft Word no meu computador",
                new Skill("Instalação", skillInformatica), Job.STATUS_OPEN);

        Job j5 = new Job(6, "O Windows não inicia após fazer alguma actualização. Alguém pode me ajudar?",
                new Skill("Reparação", skillInformatica), Job.STATUS_WORKER_SELECTED_WORKING);

        Job j6 = new Job(7, "Precisamos de um informático capaz de fazer um website para a nossa loja de sapatos",
                new Skill("Web Design", skillInformatica), Job.STATUS_WORKER_SELECTED_WORKING);


        ArrayList<Job> jobs = new ArrayList<>();
        jobs.add(j0);
        jobs.add(j1);
        jobs.add(j2);
        jobs.add(j3);
        jobs.add(j4);
        jobs.add(j5);
        jobs.add(j6);

        return jobs;

    }

    public static String generateFakeDateRecommmended(int position) {

        ArrayList<String> dates = new ArrayList<>();
        dates.add("há 1 min");
        dates.add("há 30 min");
        dates.add("há 1h");
        dates.add("há 3h");
        dates.add("há 1 dia");
        dates.add("há 2 dias");
        dates.add("há 4 dias");

        return dates.get(position);
    }


}
