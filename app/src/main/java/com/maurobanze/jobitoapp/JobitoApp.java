package com.maurobanze.jobitoapp;

import android.app.Application;

import com.maurobanze.jobitoapp.cloudApi.CloudApi;
import com.maurobanze.jobitoapp.cloudApi.CloudConnector;


public class JobitoApp extends Application {

    private static CloudConnector cloud;

    @Override
    public void onCreate() {
        super.onCreate();

        cloud = new CloudApi(getApplicationContext());
        cloud.initialize();

    }

    public static CloudConnector getCloudApiInstance(){

        return cloud;
    }

    public static final String PACKAGE_NAME = "com.maurobanze.jobitoapp";
}
