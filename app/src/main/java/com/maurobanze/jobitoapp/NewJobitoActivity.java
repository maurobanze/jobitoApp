package com.maurobanze.jobitoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;

public class NewJobitoActivity extends AppCompatActivity {

    private EditText editTextTitle;
    private EditText editTextDescription;
    private TextInputLayout textInputSkill;
    private EditText editTextSkill;
    private Spinner spinnerProvince;
    private EditText editTextReward;

    private Button buttonSave;

    private LocalDB db;

    public static final int REQUEST_PICK_SKILL = 1;
    private Skill chosenSkill;

    private MaterialDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_jobito);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(backListener);

        initViews();

    }

    private void initViews() {

        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        textInputSkill = (TextInputLayout) findViewById(R.id.textInputSkill);
        editTextSkill = (EditText) findViewById(R.id.editTextSkill);
        spinnerProvince = (Spinner) findViewById(R.id.spinnerProvince);
        editTextReward = (EditText) findViewById(R.id.editTextReward);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(clickListener);

        editTextSkill.setOnFocusChangeListener(focusListener);
        editTextSkill.setOnClickListener(clickListener);
    }

    private View.OnClickListener backListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };


    private View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if (hasFocus) {

                launchChooseSkill();

            }
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if (v == editTextSkill) {

                launchChooseSkill();
            }
            if (v == buttonSave){

                showSavingProgressDialog();
            }
        }
    };

    private void launchChooseSkill() {
        Intent intent = new Intent(NewJobitoActivity.this, SkillPickerActivity.class);
        startActivityForResult(intent, REQUEST_PICK_SKILL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_SKILL) {

            if (resultCode == Activity.RESULT_OK) {

                int skillId = data.getIntExtra(SkillPickerActivity.SKILL_PICKED, 0);
                fetchChosenSkill(skillId);
                editTextSkill.setText(chosenSkill.getName());

            }
        }

    }

    private void fetchChosenSkill(int id) {

        if (db == null)
            db = new LocalDB();

        chosenSkill = db.getSkill(id);

    }

    private void showSavingProgressDialog(){
        progressDialog = new MaterialDialog.Builder(this)
                .title("Salvando...")
                .content("Por favor aguarde")
                .progress(true, 0)
                .cancelable(true)
                .show();
    }

    private void hideProgressDialog (){

        if (progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }

    }

    private void showErrorSavingDialog(){

        progressDialog = new MaterialDialog.Builder(this)
                .title("Erro!!!")
                .content("Verifique a sua conexao a Internet e tente novamente")
                .cancelable(true)
                .positiveText("OK")
                .show();
    }

}
