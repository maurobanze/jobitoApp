package com.maurobanze.jobitoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;
import com.maurobanze.jobitoapp.views.SkillPickerView;

import java.util.ArrayList;

public class SkillPickerActivity extends AppCompatActivity implements SkillPickerView.SelectionListener {

    private SkillPickerView skillPickerView;
    private LocalDB db;

    public static final String SKILL_PICKED = "skillPicked";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill_picker);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back);
        toolbar.setNavigationOnClickListener(backListener);
        //add navigation back drawable and use here with toolbar.setNavigationIcon

        skillPickerView = (SkillPickerView) findViewById(R.id.skillPickerView);

        initDatabase();

        setupSkillPickerView();

    }

    public void initDatabase() {

        db = new LocalDB();
    }

    private void setupSkillPickerView() {

        ArrayList<SkillCategory> categories = db.fetchSkillCategoriesWithSkills();
        skillPickerView.setCategories(categories);
        skillPickerView.setSelectionListener(this);

    }

    @Override
    public void onUserSelected(@Nullable SkillCategory category, @Nullable Skill skill) {

        if (skill != null) {

            Bundle data = new Bundle();
            data.putInt(SKILL_PICKED, skill.getId());

            Intent intent = new Intent();
            intent.putExtras(data);

            setResult(Activity.RESULT_OK, intent);
            finish();

        }
    }

    private View.OnClickListener backListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            setResult(RESULT_CANCELED);
            finish();
        }
    };

}
