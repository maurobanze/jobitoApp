package com.maurobanze.jobitoapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.adapters.CreatedJobitoListAdapter;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;
import com.maurobanze.jobitoapp.views.SectionedRecyclerViewAdapter;

import java.util.ArrayList;

/**
 *
 */
public class CreatedJobitoListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_jobito_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        CreatedJobitoListAdapter contentAdapter = new CreatedJobitoListAdapter(fetchJobitos(), this);

        SectionedRecyclerViewAdapter sectionedAdapter = createSetupSectionedAdapter(contentAdapter);
        recyclerView.setAdapter(sectionedAdapter);

        return view;
    }

    private ArrayList<Job> fetchJobitos() {

        return LocalDB.generateFakeJobsCreated();

    }


    private SectionedRecyclerViewAdapter createSetupSectionedAdapter(RecyclerView.Adapter contentAdapter){

        SectionedRecyclerViewAdapter mSectionedAdapter = new
                SectionedRecyclerViewAdapter(getActivity(), R.layout.list_separator_jobito_created,
                R.id.textViewSectionTitle, contentAdapter);

        mSectionedAdapter.setSections(createListSections(null, null));
        return mSectionedAdapter;

    }
    private SectionedRecyclerViewAdapter.Section[] createListSections(ArrayList<Job> activeJobs, ArrayList<Job> closedJobs) {

        //Sectioned list
        SectionedRecyclerViewAdapter.Section[] sections = new SectionedRecyclerViewAdapter.Section[2];

        String section0Title = getString(R.string.jobito_created_active);
        String section1Title = getString(R.string.jobito_created_closed);

        sections[0] = new SectionedRecyclerViewAdapter.Section(0, section0Title);
        sections[1] = new SectionedRecyclerViewAdapter.Section(2, section1Title);

        return sections;
    }
}
