package com.maurobanze.jobitoapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.SkillPickerActivity;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.User;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;

import java.util.ArrayList;


public class MySkillsFragment extends Fragment implements View.OnClickListener {

    private ViewGroup layoutMySkills;
    private ArrayList<Skill> mySkills;

    public static final int REQUEST_PICK_SKILL = 1;
    private LocalDB db;
    private Skill chosenSkill;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_skills, container, false);
        layoutMySkills = (ViewGroup) view.findViewById(R.id.layoutMySkills);

        View fabNewSkill = view.findViewById(R.id.fabNewSkill);
        fabNewSkill.setOnClickListener(this);

        fetchMySkills();
        fillSkillsOnScreen();
        return view;
    }

    private void fetchMySkills() {

        mySkills = new ArrayList<>();

        LocalDB db = new LocalDB();
        mySkills = db.fetchSkills(new User());

    }

    private void fillSkillsOnScreen() {

        for (int i = 0; i < mySkills.size(); i++) {

            addSkill(mySkills.get(i));
        }
    }

    private void addSkill(Skill skill) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.my_skill_list_row, layoutMySkills, false);

        ImageView imageViewSkillAvatar = (ImageView) view.findViewById(R.id.imageViewSkillAvatar);
        TextView textViewSkillTitle = (TextView) view.findViewById(R.id.textViewSkillTitle);

        ImageView imageViewDeleteSkill = (ImageView) view.findViewById(R.id.buttonDeleteSkill);
        imageViewDeleteSkill.setOnClickListener(this);
        imageViewDeleteSkill.setTag(skill.getId());


        textViewSkillTitle.setText(skill.getName());

        layoutMySkills.addView(view);
    }


    private void removeSkill() {


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonDeleteSkill) {

            Toast.makeText(getActivity(), "Delete", Toast.LENGTH_SHORT).show();
            removeSkill();

        }else if (v.getId() == R.id.fabNewSkill){

            launchChooseSkill();
        }
    }

    private void launchChooseSkill() {

        Intent intent = new Intent(getActivity(), SkillPickerActivity.class);
        startActivityForResult(intent, REQUEST_PICK_SKILL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_SKILL) {

            if (resultCode == Activity.RESULT_OK) {

                int skillId = data.getIntExtra(SkillPickerActivity.SKILL_PICKED, 0);
                Skill skill = fetchChosenSkill(skillId);
                addSkill(skill);

            }
        }

    }
    private Skill fetchChosenSkill(int id) {

        if (db == null)
            db = new LocalDB();

        return db.getSkill(id);

    }


}
