package com.maurobanze.jobitoapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.adapters.RecommendedJobitoListAdapter;
import com.maurobanze.jobitoapp.entities.Skill;
import com.maurobanze.jobitoapp.entities.SkillCategory;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;
import com.maurobanze.jobitoapp.views.SkillPickerView;

import java.util.ArrayList;

/**
 *
 */
public class AllJobitoFragment extends Fragment implements SkillPickerView.SelectionListener {

    private SkillPickerView skillPickerView;
    private RecyclerView recyclerViewJobitos;

    private LocalDB db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_jobito, container, false);

        skillPickerView = (SkillPickerView) view.findViewById(R.id.skillPickerView);

        recyclerViewJobitos = new RecyclerView(getActivity());
        recyclerViewJobitos.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        recyclerViewJobitos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        initDatabase();

        setupSkillPickerView();

        return view;
    }

    public void initDatabase() {

        db = new LocalDB();
    }


    private void setupSkillPickerView() {

        ArrayList<SkillCategory> categories = db.fetchSkillCategoriesWithSkills();
        skillPickerView.setCategories(categories);
        skillPickerView.setSelectionListener(this);

    }

    @Override
    public void onUserSelected(@Nullable SkillCategory category, @Nullable Skill skill) {

        if (category != null) {// user just selected category


        } else if (skill != null) {// user selected skill

            RecommendedJobitoListAdapter adapter = new RecommendedJobitoListAdapter(LocalDB.generateFakeJobsRecommended(), this);
            recyclerViewJobitos.setAdapter(adapter);

            skillPickerView.setSelectedSkillView(recyclerViewJobitos);
        }
    }

}
