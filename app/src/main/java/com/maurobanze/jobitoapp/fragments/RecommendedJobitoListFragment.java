package com.maurobanze.jobitoapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.adapters.RecommendedJobitoListAdapter;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;

import java.util.ArrayList;

/**
 *
 */
public class RecommendedJobitoListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_jobito_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager( new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        RecommendedJobitoListAdapter adapter = new RecommendedJobitoListAdapter(fetchJobitos(), this);

        recyclerView.setAdapter(adapter);
        return view;
    }

    private ArrayList<Job> fetchJobitos (){

        return LocalDB.generateFakeJobsRecommended();

    }
}
