package com.maurobanze.jobitoapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.jobitoapp.R;
import com.maurobanze.jobitoapp.adapters.ReviewListAdapter;
import com.maurobanze.jobitoapp.entities.Hire;
import com.maurobanze.jobitoapp.entities.Job;
import com.maurobanze.jobitoapp.localDatabase.LocalDB;
import com.maurobanze.jobitoapp.utils.DividerItemDecoration;
import com.maurobanze.jobitoapp.views.SectionedRecyclerViewAdapter;

import java.util.ArrayList;

/**
 *
 */
public class MyReviewsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_my_reviews, container, false);


        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        ReviewListAdapter contentAdapter = new ReviewListAdapter(fetchReviews(), this);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this.getActivity(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        SectionedRecyclerViewAdapter sectionedAdapter = createSetupSectionedAdapter(contentAdapter);
        recyclerView.setAdapter(sectionedAdapter);

        return view;
    }

    private ArrayList<Hire> fetchReviews() {

        LocalDB fakeDb = new LocalDB();
        return fakeDb.createFakeHires();

    }


    private SectionedRecyclerViewAdapter createSetupSectionedAdapter(RecyclerView.Adapter contentAdapter){

        SectionedRecyclerViewAdapter mSectionedAdapter = new
                SectionedRecyclerViewAdapter(getActivity(), R.layout.list_separator_jobito_created,
                R.id.textViewSectionTitle, contentAdapter);

        mSectionedAdapter.setSections(createListSections(null, null));
        return mSectionedAdapter;

    }

    private SectionedRecyclerViewAdapter.Section[] createListSections(ArrayList<Job> activeJobs, ArrayList<Job> closedJobs) {

        //Sectioned list
        SectionedRecyclerViewAdapter.Section[] sections = new SectionedRecyclerViewAdapter.Section[2];

        String section0Title = getString(R.string.section_reviews_received);
        String section1Title = getString(R.string.section_reviews_created);

        sections[0] = new SectionedRecyclerViewAdapter.Section(0, section0Title);
        sections[1] = new SectionedRecyclerViewAdapter.Section(3, section1Title);

        return sections;
    }
}
