package com.maurobanze.jobitoapp.fragments;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.maurobanze.jobitoapp.NewJobitoActivity;
import com.maurobanze.jobitoapp.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NewJobitoInfoFragment extends Fragment {


    public NewJobitoInfoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_new_jobito_info, container, false);
        Button buttonNewJobito = (Button) view.findViewById(R.id.buttonNewJobito);
        buttonNewJobito.setOnClickListener(listener);

        return view;

    }

    View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(getActivity(), NewJobitoActivity.class);
            startActivity(intent);

        }
    };

}
