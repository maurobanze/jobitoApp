This app was developed as a personal project, in an attempt to turn it into a 
startup.
The idea is that skilled informal workers could be connected with people in need 
of their services, as a way to help improve earning potential for people in the
underdeveloped world, where the informal job market is the biggest employer.

App Screenshots
https://photos.app.goo.gl/QDNYhVKsZnt1LGRi8